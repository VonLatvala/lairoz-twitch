# LairOZ-Twitch

## What is this

Twitch integration for LairOZ

## How do I get set up?

These steps primarily apply to a non-Windows platform, but it is absolutely possible developing on Windows too.

* Have [yarn installed](https://yarnpkg.com/lang/en/docs/install).
* Have [git installed](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).
* (Optional, but highly recommended) Configure your editor of choice to use [ESLint](https://eslint.org/docs/user-guide/getting-started).
    * For ([Neo](https://github.com/neovim/neovim/wiki/Installing-Neovim))[Vim](https://www.vim.org/download.php) (non-neo >=8), using [Worp/Ale](https://github.com/w0rp/ale#3-installation) is highly recommended. For first time vim users, using this in conjunction with [Vim Bootstrap](https://vim-bootstrap.com/) might be a good start.
    * For [Visual Studio Code](https://code.visualstudio.com/Download), using [ESLint from the marketplace](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) is highly recommended.
    * For emacs users, `> using emacs`
* Clone this repository i.e.

```sh
git clone https://VonLatvala@bitbucket.org/VonLatvala/lairoz-twitch.git
```

* cd into the cloned folder i.e.

```sh
cd lairoz-twitch
```

* Install project dependencies i.e.

```sh
yarn install
```

* Start the appliaction

```sh
yarn start
```
